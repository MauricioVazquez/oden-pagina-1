import { Component, Input, OnInit } from '@angular/core';
import { IcardInfo } from '../../models/icard-info';

@Component({
  selector: 'app-card-info',
  templateUrl: './card-info.component.html',
  styleUrls: ['./card-info.component.scss']
})
export class CardInfoComponent implements OnInit {
  @Input() cards: IcardInfo[] = [];
  constructor() { }

  ngOnInit(): void {
  }

}

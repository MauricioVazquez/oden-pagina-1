import { Component, Input, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { ISlide } from '../../models/slide.interface';
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { AnimationsCarousel } from 'src/app/animations/animationsCarousel';

@Component({
  selector: 'app-slide-big',
  templateUrl: './slide-big.component.html',
  styleUrls: ['./slide-big.component.scss'],
  animations:[AnimationsCarousel.carousel]
})
export class SlideBigComponent implements OnInit {

  // propiedades del slide
  @Input() height;
  @Input() fullScreen = false;
  @Input() items: ISlide[] = [];

  public finalHeight: string | number = 0;
  public currentPosition = 0;
  faAngleRight = faAngleRight;
  faAngleLeft = faAngleLeft;
  carousel = true;

  constructor() { }

  ngOnInit(): void {
    // recorrer el array de items (el contenido del slide) y asignarle un id y 
    // un margen para facilitar el manejo de este
    this.items.map((i, index) => {
      i.id = index;
      i.marginLeft = 0;
    });
    // establecer el tamaño del slide
    this.finalHeight = this.fullScreen ? '100vh' : this.height;
    // recorrer el slide cada 10 segundos
    const source = timer(10000, 10000);
    const subscribe = source.subscribe(val => this.setNext());
  }

  // establecer la posicion de la imagen que se va a visualizar, recorriendo el margen -100
  setCurrentPosition(position: number) {
    this.currentPosition = position;
    this.items.find(i => i.id === 0).marginLeft = -100 * position;
  }

  // mover a la siguiente imagen
  async setNext() {
    this.carousel = !this.carousel;
    let finalPercentage = 0;
    let nextPosition = this.currentPosition + 1;
    if (nextPosition <= this.items.length - 1) {
      finalPercentage = -100 * nextPosition;
    } else {
      nextPosition = 0;
    }
    await new Promise(resolve => setTimeout
      (() => resolve(), 200)).then(() => {
        this.items.find(i => i.id === 0).marginLeft = finalPercentage;
        this.carousel = !this.carousel;
      });
    this.currentPosition = nextPosition;
  }

  //mover a la anterior imagen
   async setBack() {
    this.carousel = !this.carousel;
    let finalPercentage = 0;
    let backPosition = this.currentPosition - 1;
    if (backPosition >= 0) {
      finalPercentage = -100 * backPosition;
    } else {
      backPosition = this.items.length - 1;
      finalPercentage = -100 * backPosition;

    }
    await new Promise(resolve => setTimeout
      (() => resolve(), 200)).then(() => {
        this.items.find(i => i.id === 0).marginLeft = finalPercentage;
        this.carousel = !this.carousel;
      });
    this.currentPosition = backPosition;
  }

}

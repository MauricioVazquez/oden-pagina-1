import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideBigComponent } from './slide-big.component';

describe('SlideBigComponent', () => {
  let component: SlideBigComponent;
  let fixture: ComponentFixture<SlideBigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SlideBigComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideBigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

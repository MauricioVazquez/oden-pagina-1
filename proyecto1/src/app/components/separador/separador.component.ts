import { Component, Input, OnInit } from '@angular/core';
import { Iseparador } from '../../models/iseparador';

@Component({
  selector: 'app-separador',
  templateUrl: './separador.component.html',
  styleUrls: ['./separador.component.scss']
})
export class SeparadorComponent implements OnInit {
  @Input() center: Iseparador;
  @Input() left: Iseparador;
  @Input() right: Iseparador;
  @Input() color;

  constructor() { }

  ngOnInit(): void {
  }

}

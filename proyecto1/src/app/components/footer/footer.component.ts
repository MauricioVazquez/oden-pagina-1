import { Component, OnInit } from '@angular/core';
import { faFacebook, faTwitter, faGooglePlusG, faYoutube } from '@fortawesome/free-brands-svg-icons';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';
import { Iimages } from '../../models/iimages';
import { IMGSFLICKRDATA } from '../../models/imgsFlickrData';
import { Iseparador } from '../../models/iseparador';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faGlobe = faGlobe;
  faGooglePlusG = faGooglePlusG;
  faYoutube = faYoutube;
  // variables del separador
  left1: Iseparador = { width: '0', height: '0', color: '#FFC008' };
  center1: Iseparador = { width: '20px', height: '3px', color: '#FFC008' };
  right1: Iseparador = { width: '0', height: '0', color: '#FFC008' };
  // variables del segundo separador
  left2: Iseparador = { width: '0', height: '0', color: '#FFC008' };
  center2: Iseparador = { width: '15px', height: '3px', color: '#FFC008' };
  right2: Iseparador = { width: '30px', height: '1px', color: '#FFC008' };

  imgs: Iimages[] = IMGSFLICKRDATA;
  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { ISlide } from '../../models/slide.interface';
import { timer } from 'rxjs';
import { AnimationsCarousel } from '../../animations/animationsCarousel';


@Component({
  selector: 'app-slide',
  templateUrl: './slide.component.html',
  styleUrls: ['./slide.component.scss'],
  animations: [AnimationsCarousel.carousel]
})
export class SlideComponent implements OnInit {
  // propiedades del slide
  @Input() height;
  @Input() fullScreen = false;
  @Input() items: ISlide[] = [];
  carousel = true;

  public finalHeight: string | number = 0;
  public currentPosition = 0;


  constructor() {
  }

  ngOnInit(): void {
    // recorrer el array de items (el contenido del slide) y asignarle un id y 
    // un margen para facilitar el manejo de este
    this.items.map((i, index) => {
      i.id = index;
      i.marginLeft = 0;
    });
    // establecer el tamaño del slide
    this.finalHeight = this.fullScreen ? '100vh' : `${this.height}px`;

    // recorrer el slide cada 7 segundos
    const source = timer(7000, 7000);
    const subscribe = source.subscribe(val => this.setNext());
  }

  // establecer la posicion de la imagen que se va a visualizar, recorriendo el margen -100
  async setCurrentPosition(position: number) {
    this.carousel = !this.carousel;
    this.currentPosition = position;
    await new Promise(resolve => setTimeout
      (() => resolve(), 200)).then(() => {
        this.items.find(i => i.id === 0).marginLeft = -100 * position;
        this.carousel = !this.carousel;
      });
  }


  // mover a la siguiente imagen
  async setNext() {
    this.carousel = !this.carousel;
    let finalPercentage = 0;
    let nextPosition = this.currentPosition + 1;
    if (nextPosition <= this.items.length - 1) {
      finalPercentage = -100 * nextPosition;
    } else {
      nextPosition = 0;
    }
    await new Promise(resolve => setTimeout
      (() => resolve(), 200)).then(() => {
        this.items.find(i => i.id === 0).marginLeft = finalPercentage;
        this.carousel = !this.carousel;
      });
    this.currentPosition = nextPosition;
  }

  //mover a la anterior imagen
  async setBack() {
    this.carousel = !this.carousel;
    let finalPercentage = 0;
    let backPosition = this.currentPosition - 1;
    if (backPosition >= 0) {
      finalPercentage = -100 * backPosition;
    } else {
      backPosition = this.items.length - 1;
      finalPercentage = -100 * backPosition;

    }
    await new Promise(resolve => setTimeout
      (() => resolve(), 200)).then(() => {
        this.items.find(i => i.id === 0).marginLeft = finalPercentage;
        this.carousel = !this.carousel;
      });
    this.currentPosition = backPosition;
  }
}

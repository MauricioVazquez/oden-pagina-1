import { Component, OnInit } from '@angular/core';
import { Inavbar } from '../../models/inavbar';
import { AnimationsHeader } from '../../animations/animationsHeader';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  animations: [AnimationsHeader.nabvar]
})
export class NavbarComponent implements OnInit {
  // contenido del navbar
  dataNav: Inavbar[] = [
    {
      tittle: 'HOME', links: ['Home 1', 'Home 2', 'Home 3', 'Parallax One Page', 'Personal',
        'Slide Header', 'Angled Style'], color: 'black', width: '10%'
    },
    {
      tittle: 'PAGES', links: ['Services', 'Our Team', 'Art of Work', 'Our Capabilities', 'Features',
        'Pricing Plans', 'Left Slidebar', 'Rigth Slidebar'], color: 'black', width: '10%'
    },
    {
      tittle: 'About', links: ['About Us 1','About Us 2'], color: 'black', width: '10%'
    },
    {
      tittle: 'portafolio', links: ['Grid', 'Masonary', 'Full Page Width', 'Grid (2 Column)'], 
      color: 'black', width: '20%'
    },
    {
      tittle: 'blog', links: ['Medium Alternative', 'Small', 'Centered', 'Single Page'],
       color: '#FFC008', width: '10%'
    },
    {
      tittle: 'Shortcodes', links: ['Typography', 'Angled Sections', 'Material Icons', 'FontAwesome',
       'Animations','Embed Media', 'Grid Columns'], color: '#FFC008', width: '20%'
    },
    {
      tittle: 'Shop', links: ['Products', 'Single Page',], color: '#FFC008', width: '10%'
    },
    {
      tittle: 'contact', color: '#FFC008', width: '10%'
    },
  ];
  isLoad = true;
  //icono flecha hacia abajo
  faAngleDown = faAngleDown;

  constructor() {   }

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { IcardProduct } from '../../models/icard-product';
import { faStar } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-card-product',
  templateUrl: './card-product.component.html',
  styleUrls: ['./card-product.component.scss']
})
export class CardProductComponent implements OnInit {
  @Input() cards: IcardProduct[] = [];
  faStar = faStar;

  constructor() { }

  ngOnInit(): void {
  }
  //regresa un array de iconos de estrellas
  getStarsFill(n: number): Array<number> {
    return Array(n);
  }
  //regresa las estrellas vacias con un tamaño definido de 5
  getStarsEmpty(n: number): Array<number> {
    return Array(5 - n);
  }
}

import { Component, OnInit } from '@angular/core';
import { faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-form-contact',
  templateUrl: './form-contact.component.html',
  styleUrls: ['./form-contact.component.scss']
})
export class FormContactComponent implements OnInit {
  // iconos
  faFacebook = faFacebook;
  faTwitter = faTwitter;
  faGlobe = faGlobe;
  constructor() { }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SlideComponent } from './slide/slide.component';
import { SeparadorComponent } from './separador/separador.component';
import { CardComponent } from './card/card.component';
import { CardButtonComponent } from './card-button/card-button.component';
import { SlideBigComponent } from './slide-big/slide-big.component';
import { CardProductComponent } from './card-product/card-product.component';
import { CardInfoComponent } from './card-info/card-info.component';
import { GalleryComponent } from './gallery/gallery.component';
import { FormContactComponent } from './form-contact/form-contact.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    NavbarComponent,
    SlideComponent,
    SeparadorComponent,
    CardComponent,
    CardButtonComponent,
    SlideBigComponent,
    CardProductComponent,
    CardInfoComponent,
    GalleryComponent,
    FormContactComponent,
    FooterComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule
  ],
  exports: [
    NavbarComponent,
    SlideComponent,
    SeparadorComponent,
    CardComponent,
    CardButtonComponent,
    SlideBigComponent,
    CardProductComponent,
    CardInfoComponent,
    GalleryComponent,
    FormContactComponent,
    FooterComponent
  ]
})
export class ComponentsModule { }

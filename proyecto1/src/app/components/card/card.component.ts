import { Component, Input, OnInit } from '@angular/core';
import { library, icon } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { Icard } from '../../models/icard';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() cards: Icard[];
  icon;
  constructor() {
    library.add(fas);
  }
  ngOnInit(): void {
  }
  // recive el nombre del icon, obtiene el icon como tal y lo regresa a la vista
  getIcon(i) {
    return this.icon = icon({ prefix: 'fas', iconName: i });
  }

}

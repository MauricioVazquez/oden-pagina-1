import { Component, Input, OnInit } from '@angular/core';
import { Iseparador } from '../../models/iseparador';
import { IcardButton } from '../../models/icard-button';

@Component({
  selector: 'app-card-button',
  templateUrl: './card-button.component.html',
  styleUrls: ['./card-button.component.scss']
})
export class CardButtonComponent implements OnInit {
  // objeto card-button
  @Input() cardButton: IcardButton;
  // variables delseparador
  center: Iseparador = { width: '40px', height: '6px', color: '#FFC008' };
  left: Iseparador = { width: '0', height: '0', color: '#FFC008' };
  right: Iseparador = { width: '100px', height: '1px', color: '#FFC008' };
  constructor() { }

  ngOnInit(): void {
  }

}

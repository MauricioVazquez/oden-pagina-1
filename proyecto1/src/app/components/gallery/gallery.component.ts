import { Component, Input, OnInit } from '@angular/core';
import { AnimationsGallery } from 'src/app/animations/animationsGallery';
import { Igallery } from '../../models/igallery';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  animations: [AnimationsGallery.inOutAnimation]
})
export class GalleryComponent implements OnInit {
  @Input() gallery: Igallery;
  galleryVisible: Igallery;
  image = true;

  constructor() { }

  ngOnInit(): void {
    this.galleryVisible = Object.assign({}, this.gallery);

  }
  // filtra el arreglo de img por el id del contenedor, si el contenedor es 0 obtiene todas las imagenes
  getContenedor(c: number) {
    if (c === 0) {
      this.galleryVisible.images = this.gallery.images.filter(item => item.contenedorId !== 0);

    } else {
      this.galleryVisible.images = this.gallery.images.filter(item => item.contenedorId === c);
    }

  }
}

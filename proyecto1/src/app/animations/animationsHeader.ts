import { trigger, state, style, transition, animate } from '@angular/animations';

export const AnimationsHeader = {
    // navbar animation
    nabvar: trigger('navbar', [
        state('load', style({
            width: '50vw',

        })),
        transition('* => load', [
            animate('1.5s ease-in')
        ]),
    ]),
    // page gris al iniciar la pagina
    startPage: trigger('openPage', [
        state('open', style({
            display: 'none',
        })),
        transition('* => open', [
            animate('1s')
        ]),
    ]),
    // linea amarilla al cargar la pagina
    starLoad: trigger('loading', [
        state('load', style({
            width: '100vw',
            display: 'none',
        })),
        transition('* => load', [
            animate('1s')
        ]),
    ]),
    // tittle animation
    tittle: trigger('tittle', [
        state('load', style({
            width: '50vw',

        })),
        transition('* => load', [
            animate('1.5s ease-in')
        ]),
    ])

};

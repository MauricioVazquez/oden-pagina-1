import { state, trigger, style, transition, animate, keyframes } from '@angular/animations';

export const AnimationsGallery = {
    // gallery animation
    inOutAnimation: trigger("inOutAnimation", [
        state("in", style({ opacity: 1 })),
        transition(":enter", [
            animate(
                '0.300s ease-in-out',
                keyframes([
                    style({ opacity: 0, offset: 0 }),
                    style({ opacity: 0.25, offset: 0.25 }),
                    style({ opacity: 0.5, offset: 0.5 }),
                    style({ opacity: 0.75, offset: 0.75 }),
                    style({ opacity: 1, offset: 1 }),
                ])
            )
        ]),
        transition(":leave", [
            animate(
                400,
                keyframes([
                    style({ transform: 'scale(1)', offset: 0 }),
                    style({ opacity: 1, offset: 0 }),
                    style({ transform: 'scale(0.75)', offset: 0.25 }),
                    style({ opacity: 0.75, offset: 0.25 }),
                    style({ transform: 'scale(0.5)', offset: 0.5 }),
                    style({ opacity: 0.5, offset: 0.5 }),
                    style({ transform: 'scale(0.25)', offset: 0.75 }),
                    style({ opacity: 0.25, offset: 0.75 }),
                    style({ transform: 'scale(0)', offset: 1 }),
                    style({ opacity: 0, offset: 1 }),

                ])
            )
        ])
    ]),

};

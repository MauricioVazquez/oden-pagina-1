import { state, trigger, style, transition, animate } from '@angular/animations';

export const AnimationsCarousel = {
    // carousel animation seccion 2
    carousel: trigger('carousel', [
        state('start', style({
            opacity: 1,
        })),
        state('end', style({
            opacity: 0,

        })),
        transition('* => *', [
            animate('0.2s'),
        ]),
    ])

};

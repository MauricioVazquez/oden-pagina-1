import { Component } from '@angular/core';
import { AnimationsHeader } from './animations/animationsHeader';
import { ISlide } from './models/slide.interface';
import { SILEDATA } from './models/slideData';
import { Iseparador } from './models/iseparador';
import { Icard } from './models/icard';
import { CARDDATA } from './models/cardData';
import { IcardButton } from './models/icard-button';
import { SILEBIGDATA } from './models/slideBigData';
import { IcardProduct } from './models/icard-product';
import { CARDPRODUCTDATA } from './models/cardProductData';
import { IcardInfo } from './models/icard-info';
import { CARDINFODATA } from './models/cardInfoData';
import { GALLERYDATA } from './models/galleryData';
import { Igallery } from './models/igallery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    AnimationsHeader.startPage,
    AnimationsHeader.starLoad,
    AnimationsHeader.tittle]
})
export class AppComponent {
  // variable animacion header al iniciar la pagina
  isOpen = true;

  //cada componente de las secciones recive informacion con variables @Input con ayuda de las interfaces
  // la carpeta models estan las interfaces y las constantes con dicha informacion

  // variable del carousel
  height = 250;
  slideData: ISlide[] = SILEDATA;
  // variables del card-button -seccion 2
  cardButton: IcardButton = {
    separador: true,
    texto: '<p>Asunt in anim uis irure dolor in reprehenderit in voluptate cillum dolore eu fugiat null'
      + 'pariatuir. Exceptur sint occaecat cun proident, sunt in anim id est laborum. Allamco'
      + 'laborist nisi ut commdo consequat. Aser velit.</p><p>Maecenas nec odio et ante tincidunt tempus'
      + 'natoque penatibuset magnis dis parturient nascetur ridiculus mus.<p>',
    tittleButton: 'now more',
  };
  // variables del separador - section 2
  center2: Iseparador = { width: '40px', height: '6px', color: '#FFF' };
  left2: Iseparador = { width: '0', height: '0', color: '#FFF' };
  right2: Iseparador = { width: '100px', height: '1px', color: '#FFF' };
  // cards data -section 3
  cardsData: Icard[] = CARDDATA;
  // variables del separador - section 2
  left4: Iseparador = { width: '100px', height: '1px', color: '#FFC008' };
  center4: Iseparador = { width: '40px', height: '6px', color: '#FFC008' };
  right4: Iseparador = { width: '100px', height: '1px', color: '#FFC008' };
  // variables del card-button -seccion 4
  cardButton1: IcardButton = {
    separador: false,
    fecha: '24th august 2015',
    tittle: 'ten amazing and strange pictures ny professionals',
    texto: '<p>Donec pede justo, fingilla, aliquet nec, vuluptatee agerdiet erdiett arcu. In justo'
      + ',rhoncus ut, imperdiet a, venenatis vietardiet erde justo. llam dictum felis eu pede mollis.'
      + ' pretium diet a, venenatis vitas sto. Nullam dictum felis eu pede mollis pretiumdiet a venenatis'
      + ' ie ust. ullam dictum felis ue pedemol iumpretium leta...</p>',
    tittleButton: 'read more',
  };
  //contenido del slide big -seccion 5
  slideBigData: ISlide[] = SILEBIGDATA;
  //contenido de las targetas -seccion 6
  cardsProductsData: IcardProduct[] = CARDPRODUCTDATA;
  //contenido de las targetas -seccion 7
  cardsInfo: IcardInfo[] = CARDINFODATA;
  //contenido de la galeria (info de las imagenes) -seccion 8
  galleryData: Igallery = GALLERYDATA;
}

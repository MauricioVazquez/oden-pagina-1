import { IcardProduct } from './icard-product';
export const CARDPRODUCTDATA: IcardProduct[] = [
    {
        etiqueta: 'Best Sellers',
        img: 'assets/img/product-1.jpg',
        precio: 19.00,
        tittle: 'cotton teddy',
        estrellas: 4
    },
    {
        etiqueta: 'Best Sellers',
        img: 'assets/img/product-2.jpg',
        precio: 99.99,
        tittle: 'techie robot',
        estrellas: 4
    },
    {
        img: 'assets/img/product-3.jpg',
        precio: 79.99,
        tittle: 'vintage set',
        estrellas: 4
    },
];

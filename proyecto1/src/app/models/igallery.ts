import { Iimages } from './iimages';
import { IcontenedoresGallery } from './icontenedoresGallery';
export interface Igallery {
    contenedores: IcontenedoresGallery[];
    images: Iimages[];
}

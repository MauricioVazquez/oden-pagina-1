import { Iimages } from './iimages';
export const IMGSFLICKRDATA: Iimages[] = [
    { path: 'assets/img/flickr-1.jpg' },
    { path: 'assets/img/flickr-2.jpg' },
    { path: 'assets/img/flickr-3.jpg' },
    { path: 'assets/img/flickr-4.jpg' },
    { path: 'assets/img/flickr-5.jpg' },
    { path: 'assets/img/flickr-6.jpg' },
    { path: 'assets/img/flickr-7.jpg' },
    { path: 'assets/img/flickr-8.jpg' }
];

import { ISlide } from './slide.interface';

export const SILEDATA: ISlide[] = [
    {
        id: 1,
        img: 'assets/img/blog-medium-5.jpg'
    },
    {
        id: 2,
        img: 'assets/img/blog-medium-2.jpg'
    },
    {
        id: 3,
        img: 'assets/img/blog-medium-1.jpg'
    }
];

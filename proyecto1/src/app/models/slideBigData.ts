import { ISlide } from './slide.interface';

export const SILEBIGDATA: ISlide[] = [
    {
        id: 1,
        tittle: {
            first: 'Visual design is the process of',
            second: 'visual communication',
            third: 'and problem-solving through the use of type, space, image and color'
        },
        subtittle: '@themepassion',
        date: '1 day ago',
        img: ''
    },
    {
        id: 1,
        tittle: {
            first: 'Graphic designers use varius methods to create a',
            second: 'visual representation',
            third: 'of ideas and messeges'
        },
        subtittle: '@graphicdesign',
        date: '1 day ago',
        img: ''
    },

];

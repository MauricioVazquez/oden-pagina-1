import { Igallery } from './igallery';
export const GALLERYDATA: Igallery = {
    contenedores: [
        { contenedorId: 0, name: 'all' },
        { contenedorId: 1, name: 'development' },
        { contenedorId: 2, name: 'photography' },
        { contenedorId: 3, name: 'desing' },
        { contenedorId: 4, name: 'branding' },],
    images: [
        {
            path: 'assets/img/gallery-1.jpg',
            contenedorId: 1,
        },
        {
            path: 'assets/img/gallery-2.jpg',
            contenedorId: 2,
        },
        {
            path: 'assets/img/gallery-3.jpg',
            contenedorId: 3,
        },
        {
            path: 'assets/img/gallery-4.jpg',
            contenedorId: 4,
        },
        {
            path: 'assets/img/gallery-5.jpg',
            contenedorId: 1,
        },
        {
            path: 'assets/img/gallery-6.jpg',
            contenedorId: 2,
        },
        {
            path: 'assets/img/gallery-7.jpg',
            contenedorId: 3,
        },

        {
            path: 'assets/img/gallery-8.jpg',
            contenedorId: 4,
        }
    ]
};

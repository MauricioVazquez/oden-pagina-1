export interface ISlide {
    id: number;
    tittle?: {
        first: string;
        second?: string;
        third?: string;
    }
    subtittle?: string;
    date?: string;
    link?: string;
    img: string;
    order?: string;
    marginLeft?: number;
}
